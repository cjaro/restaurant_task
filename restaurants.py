import sys
from utility_functions import *


def get_open_restaurants(input_day, input_hour):
    restaurant_times = parse_csv()
    open_restaurants = []

    for restaurant in restaurant_times:

        try:
            convert_input_hour = hours_to_24h_time(input_hour)
        except IndexError:
            print("Invalid time format - please provide time in hh:mm format.")
            exit(1)

        restaurant_name = restaurant["name"]
        open_days = restaurant["days"]
        starting_day = open_days.split("-")[0]
        ending_day = open_days.split("-")[1]

        restaurant_open_days = weekdays_between(starting_day, ending_day)

        if input_day in restaurant_open_days:
            open_hour = hours_to_24h_time(restaurant["open"])
            close_hour = hours_to_24h_time(restaurant["closed"])

            # inclusive of opening time, exclusive to close time
            if open_hour <= convert_input_hour < close_hour:
                open_restaurants.append(restaurant_name)

    return open_restaurants


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Missing arguments - please use this format: python restaurants.py Mon 1:00 PM")
        sys.exit()

    day = sys.argv[1]
    hour = f"{sys.argv[2]} {sys.argv[3]}"

    weekday_name = get_full_weekday_name(day)

    print(f"Restaurants open on {weekday_name} at {hour}:")
    for name in get_open_restaurants(day, hour):
        print(name)
