weekdays = ["Mon", "Tues", "Wed", "Thurs", "Fri", "Sat", "Sun"]


def parse_csv():
    restaurants = []
    restaurant_headers = ("name", "days", "open", "closed")

    with open('restaurants.csv', 'r') as csv_file:
        next(csv_file)

        for row in csv_file:
            parts = row.strip().split(",")
            entry = {}

            for col, value in zip(restaurant_headers, parts):
                entry[col] = value

            restaurants.append(entry)

    return restaurants


def hours_to_24h_time(hour_to_convert):
    split_converted_hour = hour_to_convert.split(" ")
    hour_time = split_converted_hour[0]
    am_or_pm = split_converted_hour[1]
    hour_split_on_colon = hour_time.split(":")
    numeric_hour = int(hour_split_on_colon[0] + hour_split_on_colon[1])

    if am_or_pm == "PM":
        if not is_twelve_hour(numeric_hour):
            numeric_hour += 1200

    if am_or_pm == "AM":
        if is_twelve_hour(numeric_hour):
            # +1200 for close after midnightS & nothing opens @ midnight
            # if something is open past 1am, add 2400
            # find a way to compare close time (9 PM) & open time (3 AM)
            # Another edge case: what about a restaurant that's open 24hrs?
            # 12:00AM = 2400
            numeric_hour += 1200

    return numeric_hour


def is_twelve_hour(time):
    return 1200 <= time <= 1259


def weekdays_between(first_day, second_day):
    open_weekdays = []

    start_day_number = weekdays.index(first_day)
    end_day_number = weekdays.index(second_day)

    if start_day_number <= end_day_number:
        for weekday in weekdays[start_day_number:end_day_number]:
            open_weekdays.append(weekday)
    else:
        for weekday in weekdays[start_day_number:]:
            open_weekdays.append(weekday)

        for weekday in weekdays[0:end_day_number]:
            open_weekdays.append(weekday)

    return open_weekdays


def crosses_midnight_boundary(open_hour, closed_hour):
    print(open_hour, closed_hour)
    # what if a place opens at 12:30AM, or is open from 9:00PM to 9:00AM the next morning?
    # how to handle the over-midnight times


def get_full_weekday_name(weekday):
    if weekday in weekdays:
        if weekday == "Wed":
            return weekday + "nesday"

        if weekday == "Sat":
            return weekday + "urday"

        return weekday + "day"

    else:
        return "Error: Invalid weekday. Accepted format: Mon, Tues, Wed, Thurs, Fri, Sat, Sun."
