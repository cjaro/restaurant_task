# Code Challenge: Northern Brewer

Please write a basic program that accepts a day and time and displays the names of the open restaurants from a CSV file 
full of restaurant names, days open, opening times, and closing times. 

You may use any libraries you'd like. You do not need to worry about timezones or dates for this project.

## INFO:

`get_open_restaurants` takes 3 strings and returns a list of restaurant names that are open at the given time.
    `day` - a string representing a day of the week. Accepted values:
            Mon, Tues, Wed, Thurs, Fri, Sat, Sun
    `hour` - a string representing the time of day. HH:MM
    `am_pm` - a string that is either either AM or PM

Examples:
* `$ python3 restaurants.py Wed 9:00 AM`

* `$ python3 restaurants.py Thurs 11:15 AM`
> Burger Shack
> Taco Shack

* `$ python3 restaurants.py Mon 5:00 PM`
> Burger Shack
> Tokyo Sushi

* `$ python3 restaurants.py Sat 7:00 AM`
> Joy's Pancake Emporium


 ### Notes for future work:

* I could pass in optional argument of is_closing (default False) to my function hours_to_24h_time(),
   which would indicate whether or not that passed-in hour is a closing time or not. That would help
   to determine openness/closedness for places that close or open after midnight

* I want to account for times from 1200(AM) to 0059
   Example case: what if a place opens at 12:30AM, or is open from 9:00PM to 9:00AM the next morning?


